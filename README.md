# Azure Machine Learning Studio

> Découverte de la plateforme Azure Machine Learning Studio

Pour réaliser cette activité, il est nécessaire de se rendre sur la plateforme
Machine Learning d'Azure : https://ml.azure.com

(Si plusieurs _workspaces_ sont présents, choisir "p7-aml-workspace")

La documentation du Machine Learning Studio est disponible en ligne :
https://docs.microsoft.com/en-us/azure/machine-learning/
