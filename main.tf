terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.1.0"
    }
  }

  backend "http" {
  }
}

locals {
  pbayart = {
    principal_id = "cd600bde-71ef-455c-b682-53605adc405e"
  }
  rclement = {
    principal_id = "01f5bbf6-e4aa-4d76-988e-f85e88b0f83f"
  }
  grenoble_ia1_p7 = {
    principal_id = "a0c131e3-e748-461f-89a4-b344a5ed68fd"
  }
}

provider "azurerm" {
  features {
    resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

data "azurerm_client_config" "default" {}

resource "azurerm_resource_group" "default" {
  name     = "p7-aml"
  location = "France Central"
}

resource "azurerm_application_insights" "default" {
  name                = "p7-aml-insights"
  location            = azurerm_resource_group.default.location
  resource_group_name = azurerm_resource_group.default.name
  application_type    = "web"
}

resource "azurerm_key_vault" "default" {
  name                = "p7-aml-vault"
  location            = azurerm_resource_group.default.location
  resource_group_name = azurerm_resource_group.default.name
  tenant_id           = data.azurerm_client_config.default.tenant_id
  sku_name            = "standard"
}

resource "azurerm_storage_account" "default" {
  name                     = "p7amlstorage"
  location                 = azurerm_resource_group.default.location
  resource_group_name      = azurerm_resource_group.default.name
  account_tier             = "Standard"
  account_replication_type = "GRS"
}

resource "azurerm_container_registry" "default" {
  name                = "p7amlregistry"
  location            = azurerm_resource_group.default.location
  resource_group_name = azurerm_resource_group.default.name
  sku                 = "Basic"
  admin_enabled       = true
}

resource "azurerm_machine_learning_workspace" "default" {
  name                    = "p7-aml-workspace"
  location                = azurerm_resource_group.default.location
  resource_group_name     = azurerm_resource_group.default.name
  application_insights_id = azurerm_application_insights.default.id
  key_vault_id            = azurerm_key_vault.default.id
  storage_account_id      = azurerm_storage_account.default.id
  container_registry_id   = azurerm_container_registry.default.id

  identity {
    type = "SystemAssigned"
  }
}

resource "azurerm_role_assignment" "workspace_role_assignment" {
  scope                = azurerm_machine_learning_workspace.default.id
  role_definition_name = "AzureML Data Scientist"
  principal_id         = local.grenoble_ia1_p7.principal_id
}

# resource "azurerm_machine_learning_compute_instance" "default" {
#   name                          = "p7amlcomputeinstance"
#   location                      = azurerm_resource_group.default.location
#   machine_learning_workspace_id = azurerm_machine_learning_workspace.default.id
#   virtual_machine_size          = "Standard_DS11_v2"
# 
#   assign_to_user {
#     object_id = local.rclement.principal_id
#     tenant_id = data.azurerm_client_config.default.tenant_id
#   }
# 
#   identity {
#     type = "SystemAssigned"
#   }
# }

resource "azurerm_machine_learning_compute_cluster" "default" {
  name                          = "p7amlcomputecluster"
  location                      = azurerm_resource_group.default.location
  machine_learning_workspace_id = azurerm_machine_learning_workspace.default.id
  vm_priority                   = "Dedicated"
  vm_size                       = "Standard_F4s_v2"

  identity {
    type = "SystemAssigned"
  }

  scale_settings {
    min_node_count                       = 0
    max_node_count                       = 2
    scale_down_nodes_after_idle_duration = "PT120S" # 120 seconds
  }
}
